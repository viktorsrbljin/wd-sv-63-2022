const firebaseConfig = {
  apiKey: "AIzaSyC6faK1m1laCTZWsIdz0RLVH1F9civYDTU",
  authDomain: "turisticka-agencija-ac542.firebaseapp.com",
  databaseURL: "https://turisticka-agencija-ac542-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "turisticka-agencija-ac542",
  storageBucket: "turisticka-agencija-ac542.appspot.com",
  messagingSenderId: "395723141759",
  appId: "1:395723141759:web:b92d75933d48550146c54a",
  measurementId: "G-GXTFP2TW1E"
};


firebase.initializeApp(firebaseConfig);
var database = firebase.database();


function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}
updateUserImage()

function submitLoginForm(event) {
event.preventDefault();
document.getElementById('KorisnickoImeErrorMessage').style.display = 'none';
document.getElementById('LozinkaErrorMessage').style.display = 'none';
document.getElementById('loginSuccessMessage').style.display = 'none';

var username = document.getElementById('username').value;
var password = document.getElementById('password').value;

var loginData = {
  username: username,
  password: password
};


var korisniciRef = database.ref('korisnici');
korisniciRef.once('value', function(snapshot) {
  var korisnici = snapshot.val();

  var usernameExists = false;
  var correctPassword = false;

  
  for (var key in korisnici) {
    if (korisnici.hasOwnProperty(key)) {
      var korisnik = korisnici[key];

      
      if (korisnik.korisnickoIme === username) {
        usernameExists = true;

      
        if (korisnik.lozinka === password) {
          correctPassword = true;
          break;
        }
      }
    }
  }

  if (!usernameExists) {
    document.getElementById('KorisnickoImeErrorMessage').style.display = 'block';
  } else if (!correctPassword) {
    document.getElementById('LozinkaErrorMessage').style.display = 'block';
  } else {
    window.loggedIn = true;
    $('#loginModal').modal('hide');
    $('#korisnikModal').modal('show');
    updateUserImage()
  }
});
}
document.getElementById('loginBtn').addEventListener('click', submitLoginForm);

function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}


document.getElementById("logoutBtn").addEventListener("click", function() {
window.loggedIn = false;
updateUserImage();
$('#korisnikModal').modal('hide');
});

function submitRegisterForm(event) {
  event.preventDefault(); 

  var usernameR = document.getElementById('usernameR').value;
  var passwordR = document.getElementById('passwordR').value;
  var imeR = document.getElementById('imeR').value;
  var prezimeR = document.getElementById('prezimeR').value;
  var emailR = document.getElementById('emailR').value;
  var rodjenjeR = document.getElementById('rodjenjeR').value;
  var adresaR = document.getElementById('adresaR').value;
  var telefonR = document.getElementById('telefonR').value;

  var korisnikData = {
    korisnickoIme: usernameR,
    lozinka: passwordR,
    ime: imeR,
    prezime: prezimeR,
    email: emailR,
    datumRodjenja: rodjenjeR,
    adresa: adresaR,
    telefon: telefonR,
  };

  database.ref('korisnici').push(korisnikData)
    .then(function() {
      console.log('Form data saved successfully.');
      document.getElementById('registerForm').reset(); 
      document.getElementById('successRegister').style.display = 'block'; 
    })
    .catch(function(error) {
      console.error('Error saving form data: ', error);
    });
}
document.getElementById('registerBtn').addEventListener('click', submitRegisterForm);

const urlParams = new URLSearchParams(window.location.search);
const destinacijaID = urlParams.get('destinacijaID');

const databaseRef = firebase.database().ref('destinacije');

databaseRef.once('value')
  .then((snapshot) => {
    let foundData = null;

    snapshot.forEach((destinacijaSnapshot) => {
      const destinacijaData = destinacijaSnapshot.val();

      Object.entries(destinacijaData).forEach(([nestedKey, nestedData]) => {
        if (nestedKey === destinacijaID) {
          foundData = nestedData;
        }
      });
    });

    if (foundData) {
        const tipInput = document.getElementById('tip');
        tipInput.value = foundData.tip;
        const prevozInput = document.getElementById('prevoz');
        prevozInput.value = foundData.prevoz;
        const cenaInput = document.getElementById('cena');
        cenaInput.value = foundData.cena;
        const maxOsobaInput = document.getElementById('maxOsoba');
        maxOsobaInput.value = foundData.maxOsoba;
        const opisElement = document.getElementById('oDestinaciji');
        opisElement.textContent = foundData.opis;

        const slikeContainer = document.getElementById('slikeContainer');
      foundData.slike.forEach((slika, index) => {
        const formGroup = document.createElement('div');
        formGroup.classList.add('form-group', 'row');

        const label = document.createElement('label');
        label.setAttribute('for', `link${index + 1}`);
        label.classList.add('col-sm-3', 'col-md-3', 'col-form-label');
        label.textContent = `Slika ${index + 1}:`;

        const inputDiv = document.createElement('div');
        inputDiv.classList.add('col-sm-9', 'col-lg-6');

        const input = document.createElement('input');
        input.setAttribute('type', 'url');
        input.setAttribute('class', 'form-control');
        input.setAttribute('id', `link${index + 1}`);
        input.setAttribute('placeholder', 'Unesi link');
        input.setAttribute('value', slika);

        inputDiv.appendChild(input);

        formGroup.appendChild(label);
        formGroup.appendChild(inputDiv);

        slikeContainer.appendChild(formGroup);
      });
    } else {
      console.log('Nema podataka za datu agenciju');
    }
  })
  .catch((error) => {
    console.log('Greska pri pribavljanju podataka:', error);
  });


  document.getElementById('azurirajBtn').addEventListener('click', function(event) {
    event.preventDefault();
  
    const destinacijeRef = database.ref('destinacije');
  
    destinacijeRef.once('value', (snapshot) => {
      snapshot.forEach((parentSnapshot) => {
        parentSnapshot.forEach((childSnapshot) => {
          if (childSnapshot.key === destinacijaID) {
            const nestedChildRef = childSnapshot.ref;
            console.log('Uspesno nadjen ref:', nestedChildRef.key);
            updateDestinacija(nestedChildRef);
          }
        });
      });
    });
  
  });
  
  function updateDestinacija(destinacijaRef) {
    const tip = document.getElementById('tip').value.trim();
    const prevoz = document.getElementById('prevoz').value.trim();
    const cena = document.getElementById('cena').value.trim();
    const maxOsoba = document.getElementById('maxOsoba').value.trim();
  
    const lettersOnlyRegex = /^[A-Za-z\s]+$/;
    const numbersOnlyRegex = /^\d+$/;
  
    if (tip === '') {
      displayErrorMessage('Tip polje je obavezno.');
      return;
    } else if (!lettersOnlyRegex.test(tip)) {
      displayErrorMessage('Tip polje može sadržati samo slova.');
      return;
    }
  
    if (prevoz === '') {
      displayErrorMessage('Prevoz polje je obavezno.');
      return;
    } else if (!lettersOnlyRegex.test(prevoz)) {
      displayErrorMessage('Prevoz polje može sadržati samo slova.');
      return;
    }
  
    if (cena === '') {
      displayErrorMessage('Cena polje je obavezno.');
      return;
    } else if (!numbersOnlyRegex.test(cena)) {
      displayErrorMessage('Cena polje može sadržati samo brojeve.');
      return;
    }
  
    if (maxOsoba === '') {
      displayErrorMessage('Max osoba polje je obavezno.');
      return;
    } else if (!numbersOnlyRegex.test(maxOsoba)) {
      displayErrorMessage('Max osoba polje može sadržati samo brojeve.');
      return;
    }
  
    destinacijaRef.update({
      tip: tip,
      prevoz: prevoz,
      cena: cena,
      maxOsoba: maxOsoba
    }, function(error) {
      if (error) {
        document.getElementById('errorMessage').textContent = 'Došlo je do greške prilikom ažuriranja podataka.';
        document.getElementById('errorMessage').style.display = 'block';
      } else {
        document.getElementById('successMessage').style.display = 'block';
      }
    });
  }