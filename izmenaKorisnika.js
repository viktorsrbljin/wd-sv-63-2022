const firebaseConfig = {
  apiKey: "AIzaSyC6faK1m1laCTZWsIdz0RLVH1F9civYDTU",
  authDomain: "turisticka-agencija-ac542.firebaseapp.com",
  databaseURL: "https://turisticka-agencija-ac542-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "turisticka-agencija-ac542",
  storageBucket: "turisticka-agencija-ac542.appspot.com",
  messagingSenderId: "395723141759",
  appId: "1:395723141759:web:b92d75933d48550146c54a",
  measurementId: "G-GXTFP2TW1E"
};


firebase.initializeApp(firebaseConfig);
var database = firebase.database();


function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}
updateUserImage()

function submitLoginForm(event) {
event.preventDefault();
document.getElementById('KorisnickoImeErrorMessage').style.display = 'none';
document.getElementById('LozinkaErrorMessage').style.display = 'none';
document.getElementById('loginSuccessMessage').style.display = 'none';

var username = document.getElementById('username').value;
var password = document.getElementById('password').value;

var loginData = {
  username: username,
  password: password
};


var korisniciRef = database.ref('korisnici');
korisniciRef.once('value', function(snapshot) {
  var korisnici = snapshot.val();

  var usernameExists = false;
  var correctPassword = false;

  
  for (var key in korisnici) {
    if (korisnici.hasOwnProperty(key)) {
      var korisnik = korisnici[key];

      
      if (korisnik.korisnickoIme === username) {
        usernameExists = true;

      
        if (korisnik.lozinka === password) {
          correctPassword = true;
          break;
        }
      }
    }
  }

  if (!usernameExists) {
    document.getElementById('KorisnickoImeErrorMessage').style.display = 'block';
  } else if (!correctPassword) {
    document.getElementById('LozinkaErrorMessage').style.display = 'block';
  } else {
    window.loggedIn = true;
    $('#loginModal').modal('hide');
    $('#korisnikModal').modal('show');
    updateUserImage()
  }
});
}
document.getElementById('loginBtn').addEventListener('click', submitLoginForm);

function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}


document.getElementById("logoutBtn").addEventListener("click", function() {
window.loggedIn = false;
updateUserImage();
$('#korisnikModal').modal('hide');
});

function submitRegisterForm(event) {
  event.preventDefault(); 

  var usernameR = document.getElementById('usernameR').value;
  var passwordR = document.getElementById('passwordR').value;
  var imeR = document.getElementById('imeR').value;
  var prezimeR = document.getElementById('prezimeR').value;
  var emailR = document.getElementById('emailR').value;
  var rodjenjeR = document.getElementById('rodjenjeR').value;
  var adresaR = document.getElementById('adresaR').value;
  var telefonR = document.getElementById('telefonR').value;

  var korisnikData = {
    korisnickoIme: usernameR,
    lozinka: passwordR,
    ime: imeR,
    prezime: prezimeR,
    email: emailR,
    datumRodjenja: rodjenjeR,
    adresa: adresaR,
    telefon: telefonR,
  };

  database.ref('korisnici').push(korisnikData)
    .then(function() {
      console.log('Form data saved successfully.');
      document.getElementById('registerForm').reset(); 
      document.getElementById('successRegister').style.display = 'block'; 
    })
    .catch(function(error) {
      console.error('Error saving form data: ', error);
    });
}
document.getElementById('registerBtn').addEventListener('click', submitRegisterForm);


  document.getElementById('nadjiBtn').addEventListener('click', function(event) {
    event.preventDefault();
    document.getElementById('successMessageDelete').style.display = 'none';
    document.getElementById('errorMessage').style.display = 'none';
    document.getElementById('successMessage').style.display = 'none'; 

    var korisnickoIme = document.getElementById('korisnickoImeI').value;
    var database = firebase.database();
    var korisnikRef = database.ref('korisnici').orderByChild('korisnickoIme').equalTo(korisnickoIme).limitToFirst(1);

    korisnikRef.once('value', function(snapshot) {
      var korisnikData = snapshot.val();

      if (korisnikData) {
        var korisnikID = Object.keys(korisnikData)[0];

        document.getElementById('imeI').value = korisnikData[korisnikID].ime;
        document.getElementById('adresaI').value = korisnikData[korisnikID].adresa;
        document.getElementById('datumRodjenjaI').value = korisnikData[korisnikID].datumRodjenja;
        document.getElementById('lozinkaI').value = korisnikData[korisnikID].lozinka;
        document.getElementById('telefonI').value = korisnikData[korisnikID].telefon;
        document.getElementById('emailI').value = korisnikData[korisnikID].email;
        document.getElementById('prezimeI').value = korisnikData[korisnikID].prezime;
        document.getElementById('podaciKorisnikaForm').style.display = 'block';

      } else {
        alert('Korisnik nije nadjen!');
      }
    });
  });

  document.getElementById('azurirajBtn').addEventListener('click', function(event) {
    event.preventDefault(); 
    document.getElementById('successMessageDelete').style.display = 'none';
    document.getElementById('errorMessage').style.display = 'none';
    document.getElementById('successMessage').style.display = 'none';
  
    var korisnickoIme = document.getElementById('korisnickoImeI').value.trim();
    var ime = document.getElementById('imeI').value.trim();
    var adresa = document.getElementById('adresaI').value.trim();
    var datumRodjenja = document.getElementById('datumRodjenjaI').value.trim();
    var lozinka = document.getElementById('lozinkaI').value.trim();
    var telefon = document.getElementById('telefonI').value.trim();
    var email = document.getElementById('emailI').value.trim();
    var prezime = document.getElementById('prezimeI').value.trim();
    
    if (ime === '' || adresa === '' || datumRodjenja === '' || lozinka === '' || telefon === '' || email === '' || prezime === '') {
      document.getElementById('errorMessage').textContent = 'Popunite sva polja.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
    
    var imeRegex = /^[a-zA-Z\s]+$/;
    if (!imeRegex.test(ime)) {
      document.getElementById('errorMessage').textContent = 'Ime mora sadržati samo slova i razmak eventualno.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var prezimeRegex =/^[a-zA-Z]+$/;
    if (!prezimeRegex.test(prezime)) {
      document.getElementById('errorMessage').textContent = 'Prezime mora sadržati samo slova.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var telefonRegex = /^[^a-zA-Z]+$/;
    if (!telefonRegex.test(telefon)) {
      document.getElementById('errorMessage').textContent = 'Telefon mora sadržati samo brojeve i znak +.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      document.getElementById('errorMessage').textContent = 'Email adresa mora biti validna.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    if (lozinka.length < 8) {
      document.getElementById('errorMessage').textContent = 'Lozinka mora imati minimum 8 karaktera.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var korisnikRef = database.ref('korisnici').orderByChild('korisnickoIme').equalTo(korisnickoIme).limitToFirst(1);
    
    korisnikRef.once('value', function(snapshot) {
      if (snapshot.exists()) {
        snapshot.forEach(function(childSnapshot) {
          var korisnikKey = childSnapshot.key;
          var korisnikRefToUpdate = database.ref('korisnici/' + korisnikKey);
          
          korisnikRefToUpdate.update({
            ime: ime,
            adresa: adresa,
            datumRodjenja: datumRodjenja,
            lozinka: lozinka,
            telefon: telefon,
            email: email,
            prezime: prezime
          }, function(error) {
            if (error) {
              document.getElementById('errorMessage').textContent = 'Došlo je do greške, probajte ponovo.';
              document.getElementById('errorMessage').style.display = 'block';
            } else {
              document.getElementById('successMessage').style.display = 'block';
            }
          });
        });
      } else {
        document.getElementById('errorMessage').textContent = 'Korisnik sa zadatim korisničkim imenom ne postoji.';
        document.getElementById('errorMessage').style.display = 'block';
      }
    });
  });

 document.getElementById("deleteBtn").addEventListener("click", function(event) {
  event.preventDefault(); 
  document.getElementById('successMessageDelete').style.display = 'none';
  document.getElementById('errorMessage').style.display = 'none';
  document.getElementById('successMessage').style.display = 'none';
  
 
  $('#confirmationModal').modal('show');
});

document.getElementById('confirmDeleteBtn').addEventListener('click', function() {
  $('#confirmationModal').modal('hide');

  var korisnickoIme = document.getElementById('korisnickoImeI').value;
  var database = firebase.database();
  var korisnikRef = database.ref('korisnici');

  korisnikRef.orderByChild('korisnickoIme').equalTo(korisnickoIme).once('value')
    .then(function(snapshot) {
      if (snapshot.exists()) {
        var korisnikID = Object.keys(snapshot.val())[0];

        korisnikRef.child(korisnikID).remove()
          .then(function() {
            document.getElementById('successMessageDelete').style.display = 'block';
            document.getElementById('errorMessageDelete').style.display = 'none';
          })
          .catch(function(error) {
            document.getElementById('errorMessageDelete').textContent = "Greška pri uklanjanju korisnika: " + error;
            document.getElementById('errorMessageDelete').style.display = 'block';
            document.getElementById('successMessageDelete').style.display = 'none';
          });
      } else {
        document.getElementById('errorMessageDelete').textContent = "Korisničko ime ne postoji.";
        document.getElementById('errorMessageDelete').style.display = 'block';
        document.getElementById('successMessageDelete').style.display = 'none';
      }
    })
    .catch(function(error) {
      document.getElementById('errorMessageDelete').textContent = "Greška pri pretraživanju korisnika: " + error;
      document.getElementById('errorMessageDelete').style.display = 'block';
      document.getElementById('successMessageDelete').style.display = 'none';
    });
});

    

