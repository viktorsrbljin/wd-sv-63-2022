const firebaseConfig = {
  apiKey: "AIzaSyC6faK1m1laCTZWsIdz0RLVH1F9civYDTU",
  authDomain: "turisticka-agencija-ac542.firebaseapp.com",
  databaseURL: "https://turisticka-agencija-ac542-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "turisticka-agencija-ac542",
  storageBucket: "turisticka-agencija-ac542.appspot.com",
  messagingSenderId: "395723141759",
  appId: "1:395723141759:web:b92d75933d48550146c54a",
  measurementId: "G-GXTFP2TW1E"
};


firebase.initializeApp(firebaseConfig);
var database = firebase.database();


function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}
updateUserImage()

function submitLoginForm(event) {
event.preventDefault();
document.getElementById('KorisnickoImeErrorMessage').style.display = 'none';
document.getElementById('LozinkaErrorMessage').style.display = 'none';
document.getElementById('loginSuccessMessage').style.display = 'none';

var username = document.getElementById('username').value;
var password = document.getElementById('password').value;

var loginData = {
  username: username,
  password: password
};


var korisniciRef = database.ref('korisnici');
korisniciRef.once('value', function(snapshot) {
  var korisnici = snapshot.val();

  var usernameExists = false;
  var correctPassword = false;

  
  for (var key in korisnici) {
    if (korisnici.hasOwnProperty(key)) {
      var korisnik = korisnici[key];

      
      if (korisnik.korisnickoIme === username) {
        usernameExists = true;

      
        if (korisnik.lozinka === password) {
          correctPassword = true;
          break;
        }
      }
    }
  }

  if (!usernameExists) {
    document.getElementById('KorisnickoImeErrorMessage').style.display = 'block';
  } else if (!correctPassword) {
    document.getElementById('LozinkaErrorMessage').style.display = 'block';
  } else {
    window.loggedIn = true;
    $('#loginModal').modal('hide');
    $('#korisnikModal').modal('show');
    updateUserImage()
  }
});
}
document.getElementById('loginBtn').addEventListener('click', submitLoginForm);

function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}


document.getElementById("logoutBtn").addEventListener("click", function() {
window.loggedIn = false;
updateUserImage();
$('#korisnikModal').modal('hide');
});

function submitRegisterForm(event) {
  event.preventDefault(); 

  var usernameR = document.getElementById('usernameR').value;
  var passwordR = document.getElementById('passwordR').value;
  var imeR = document.getElementById('imeR').value;
  var prezimeR = document.getElementById('prezimeR').value;
  var emailR = document.getElementById('emailR').value;
  var rodjenjeR = document.getElementById('rodjenjeR').value;
  var adresaR = document.getElementById('adresaR').value;
  var telefonR = document.getElementById('telefonR').value;

  var korisnikData = {
    korisnickoIme: usernameR,
    lozinka: passwordR,
    ime: imeR,
    prezime: prezimeR,
    email: emailR,
    datumRodjenja: rodjenjeR,
    adresa: adresaR,
    telefon: telefonR,
  };

  database.ref('korisnici').push(korisnikData)
    .then(function() {
      console.log('Form data saved successfully.');
      document.getElementById('registerForm').reset(); 
      document.getElementById('successRegister').style.display = 'block'; 
    })
    .catch(function(error) {
      console.error('Error saving form data: ', error);
    });
}
document.getElementById('registerBtn').addEventListener('click', submitRegisterForm);


  document.getElementById('nadjiBtn').addEventListener('click', function(event) {
    event.preventDefault();
    document.getElementById('errorMessageDelete').style.display = 'none';
    document.getElementById('successMessageDelete').style.display = 'none';
    document.getElementById('errorMessage').style.display = 'none';
    document.getElementById('successMessage').style.display = 'none';

    var agencijaName = document.getElementById('agencija').value;
    var database = firebase.database();
    var agencijaRef = database.ref('agencije').orderByChild('naziv').equalTo(agencijaName).limitToFirst(1);

    agencijaRef.once('value', function(snapshot) {
      var agencijaData = snapshot.val();

      if (agencijaData) {
        var agencijaID = Object.keys(agencijaData)[0];

        document.getElementById('nazivA').value = agencijaData[agencijaID].naziv;
        document.getElementById('adresaA').value = agencijaData[agencijaID].adresa;
        document.getElementById('godinaA').value = agencijaData[agencijaID].godina;
        document.getElementById('slikaA').value = agencijaData[agencijaID].logo;
        document.getElementById('telefonA').value = agencijaData[agencijaID].brojTelefona;
        document.getElementById('emailA').value = agencijaData[agencijaID].email;
        document.getElementById('destinacijeA').value = agencijaData[agencijaID].destinacije;
        document.getElementById('podaciAgencijeForm').style.display = 'block';

      } else {
        alert('Agencija nije nadjena!');
      }
    });
  });


  document.getElementById('azurirajAgenciju').addEventListener('click', function(event) {
    event.preventDefault(); 
    document.getElementById('errorMessageDelete').style.display = 'none';
    document.getElementById('successMessageDelete').style.display = 'none';
    document.getElementById('errorMessage').style.display = 'none';
    document.getElementById('successMessage').style.display = 'none';
    var database = firebase.database();
  
    var naziv = document.getElementById('nazivA').value.trim();
    var adresa = document.getElementById('adresaA').value.trim();
    var godina = document.getElementById('godinaA').value.trim();
    var slika = document.getElementById('slikaA').value.trim();
    var telefon = document.getElementById('telefonA').value.trim();
    var email = document.getElementById('emailA').value.trim();
    var destinacije = document.getElementById('destinacijeA').value.trim();
  
    if (naziv === '' || adresa === '' || godina === '' || slika === '' || telefon === '' || email === '' || destinacije === '') {
      document.getElementById('successMessage').style.display = 'none';
      document.getElementById('errorMessage').textContent = 'Molimo vas popunite sva polja.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
  
    var nazivRegex = /^[a-zA-Z\s]+$/;
    if (!nazivRegex.test(naziv)) {
      document.getElementById('errorMessage').textContent = 'Naziv mora biti sastavljen od slova i imati najviše 16 slova.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var godinaRegex = /^\d{1,4}$/;
    if (!godinaRegex.test(godina)) {
      document.getElementById('errorMessage').textContent = 'Godina mora biti broj i imati najviše 4 cifre.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var slikaRegex = /^https?:\/\/[^\s/$.?#].[^\s]*$/;
    if (!slikaRegex.test(slika)) {
      document.getElementById('errorMessage').textContent = 'Slika mora biti validan URL.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var telefonRegex = /^[0-9+\-/]+$/;
    if (!telefonRegex.test(telefon)) {
      document.getElementById('errorMessage').textContent = 'Telefon mora sadržati samo brojeve i znak +.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      document.getElementById('errorMessage').textContent = 'Email adresa mora biti validna.';
      document.getElementById('errorMessage').style.display = 'block';
      return;
    }
  
    var agencijaRef = database.ref('agencije').orderByChild('naziv').equalTo(naziv).limitToFirst(1);
  
    agencijaRef.once('value', function(snapshot) {
      if (snapshot.exists()) {
        snapshot.forEach(function(childSnapshot) {
          var agencijaKey = childSnapshot.key;
          var agencijaRefToUpdate = database.ref('agencije/' + agencijaKey);
  
          agencijaRefToUpdate.update({
            naziv: naziv,
            adresa: adresa,
            godina: godina,
            slika: slika,
            telefon: telefon,
            email: email,
            destinacije: destinacije
          }, function(error) {
            if (error) {
              document.getElementById('errorMessage').textContent = 'Došlo je do greške prilikom ažuriranja podataka.';
              document.getElementById('errorMessage').style.display = 'block';
            } else {
              document.getElementById('successMessage').textContent = 'Podaci su uspešno ažurirani.';
              document.getElementById('successMessage').style.display = 'block';
            }
          });
        });
      } else {
        document.getElementById('errorMessage').textContent = 'Agencija sa zadatim nazivom ne postoji.';
        document.getElementById('errorMessage').style.display = 'block';
      }
    });
  });


document.getElementById("deleteBtn").addEventListener("click", function(event) {
  event.preventDefault();


  $('#confirmationModal').modal('show');
});

document.getElementById("confirmDeleteBtn").addEventListener("click", function(event) {
  event.preventDefault();


  $('#confirmationModal').modal('hide');

  var agencija = document.getElementById('agencija').value;
  var database = firebase.database();
  var agencijaRef = database.ref('agencije');

  agencijaRef.orderByChild('naziv').equalTo(agencija).once('value')
    .then(function(snapshot) {
      if (snapshot.exists()) {
        var agencijaID = Object.keys(snapshot.val())[0];

        agencijaRef.child(agencijaID).remove()
          .then(function() {
            document.getElementById('successMessageDelete').style.display = 'block';
            document.getElementById('errorMessageDelete').style.display = 'none';
          })
          .catch(function(error) {
            document.getElementById('errorMessageDelete').textContent = "Greška pri uklanjanju agencije: " + error;
            document.getElementById('errorMessageDelete').style.display = 'block';
            document.getElementById('successMessageDelete').style.display = 'none';
          });
      } else {
        document.getElementById('errorMessageDelete').textContent = "Agencija ne postoji.";
        document.getElementById('errorMessageDelete').style.display = 'block';
        document.getElementById('successMessageDelete').style.display = 'none';
      }
    })
    .catch(function(error) {
      document.getElementById('errorMessageDelete').textContent = "Greška pri pretraživanju agencije: " + error;
      document.getElementById('errorMessageDelete').style.display = 'block';
      document.getElementById('successMessageDelete').style.display = 'none';
    });
});
