const firebaseConfig = {
  apiKey: "AIzaSyC6faK1m1laCTZWsIdz0RLVH1F9civYDTU",
  authDomain: "turisticka-agencija-ac542.firebaseapp.com",
  databaseURL: "https://turisticka-agencija-ac542-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "turisticka-agencija-ac542",
  storageBucket: "turisticka-agencija-ac542.appspot.com",
  messagingSenderId: "395723141759",
  appId: "1:395723141759:web:b92d75933d48550146c54a",
  measurementId: "G-GXTFP2TW1E"
};


firebase.initializeApp(firebaseConfig);
var database = firebase.database();


function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}
updateUserImage()

function submitLoginForm(event) {
event.preventDefault();
document.getElementById('KorisnickoImeErrorMessage').style.display = 'none';
document.getElementById('LozinkaErrorMessage').style.display = 'none';
document.getElementById('loginSuccessMessage').style.display = 'none';

var username = document.getElementById('username').value;
var password = document.getElementById('password').value;

var loginData = {
  username: username,
  password: password
};


var korisniciRef = database.ref('korisnici');
korisniciRef.once('value', function(snapshot) {
  var korisnici = snapshot.val();

  var usernameExists = false;
  var correctPassword = false;

  
  for (var key in korisnici) {
    if (korisnici.hasOwnProperty(key)) {
      var korisnik = korisnici[key];

      
      if (korisnik.korisnickoIme === username) {
        usernameExists = true;

      
        if (korisnik.lozinka === password) {
          correctPassword = true;
          break;
        }
      }
    }
  }

  if (!usernameExists) {
    document.getElementById('KorisnickoImeErrorMessage').style.display = 'block';
  } else if (!correctPassword) {
    document.getElementById('LozinkaErrorMessage').style.display = 'block';
  } else {
    window.loggedIn = true;
    $('#loginModal').modal('hide');
    $('#korisnikModal').modal('show');
    updateUserImage()
  }
});
}
document.getElementById('loginBtn').addEventListener('click', submitLoginForm);

function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}


document.getElementById("logoutBtn").addEventListener("click", function() {
window.loggedIn = false;
updateUserImage();
$('#korisnikModal').modal('hide');
});

function submitRegisterForm(event) {
  event.preventDefault(); 

  var usernameR = document.getElementById('usernameR').value;
  var passwordR = document.getElementById('passwordR').value;
  var imeR = document.getElementById('imeR').value;
  var prezimeR = document.getElementById('prezimeR').value;
  var emailR = document.getElementById('emailR').value;
  var rodjenjeR = document.getElementById('rodjenjeR').value;
  var adresaR = document.getElementById('adresaR').value;
  var telefonR = document.getElementById('telefonR').value;

  var korisnikData = {
    korisnickoIme: usernameR,
    lozinka: passwordR,
    ime: imeR,
    prezime: prezimeR,
    email: emailR,
    datumRodjenja: rodjenjeR,
    adresa: adresaR,
    telefon: telefonR,
  };

  database.ref('korisnici').push(korisnikData)
    .then(function() {
      console.log('Form data saved successfully.');
      document.getElementById('registerForm').reset(); 
      document.getElementById('successRegister').style.display = 'block'; 
    })
    .catch(function(error) {
      console.error('Error saving form data: ', error);
    });
}
document.getElementById('registerBtn').addEventListener('click', submitRegisterForm);

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const agencijaID = urlParams.get('agencijaID');

const agencijaRef = firebase.database().ref(`agencije/${agencijaID}`);


agencijaRef.once('value')
  .then((snapshot) => {
    const agencijaData = snapshot.val();
    const container = document.getElementById('agencija');
    const tableID = document.getElementById('table');

    const col1 = document.createElement('div');
    col1.classList.add('col-md-5', 'order-md-1', 'd-flex', 'justify-content-center');
    col1.style.paddingTop = '5rem';
    col1.style.paddingBottom = '5rem';

    const img = document.createElement('img');
    img.src = agencijaData.logo;
    img.classList.add('img-fluid', 'rounded');
    img.alt = 'Agency Logo';
    img.style.maxWidth = '100%';
    img.style.height = 'auto';
    img.style.maxHeight = '300px';
    img.style.border = '5px solid rgb(27, 53, 170)';

    col1.appendChild(img);

    const col2 = document.createElement('div');
    col2.classList.add('col-md-5', 'order-md-1', 'd-flex', 'align-items-center');
    col2.style.paddingTop = '3rem';
    col2.style.paddingBottom = '5rem';

    const textContainer = document.createElement('div');
    textContainer.classList.add('text-white', 'shadow-text', 'mx-auto');

    const agencyName = document.createElement('h1');
    agencyName.textContent = agencijaData.naziv;

    const agencyDetails = document.createElement('p');
    agencyDetails.id = 'podaci';
    agencyDetails.innerHTML = `
      ${agencijaData.godina}<br>
      ${agencijaData.email}<br>
      ${agencijaData.brojTelefona}
    `;

    textContainer.appendChild(agencyName);
    textContainer.appendChild(agencyDetails);
    col2.appendChild(textContainer);

    
    const table = document.createElement('table');
    table.classList.add('table', 'table-bordered', 'text-center');

    const thead = document.createElement('thead');
    const tr = document.createElement('tr');
    const th = document.createElement('th');
    th.textContent = 'Destinacije';

    tr.appendChild(th);
    thead.appendChild(tr);

    const tbody = document.createElement('tbody');
    tbody.id = 'korisnikTableBody';

    
    const databaseDestinacijeRef = firebase.database().ref('destinacije');

    databaseDestinacijeRef.once('value', snapshot => {
      snapshot.forEach(childSnapshot => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();
    
        if (childKey === agencijaData.destinacije) {
          Object.keys(childData).forEach(nestedKey => {
            const nestedChild = childData[nestedKey];
            const naziv = nestedChild.naziv; 
            const destinationRow = document.createElement('tr');
            destinationRow.classList.add('hover-link');
            destinationRow.addEventListener('click', () => {
              window.location.href = `destinacija.html?destinacijaID=${nestedKey}`;
            });
            const destinationCell = document.createElement('td');
            destinationCell.textContent = naziv;
            destinationRow.appendChild(destinationCell);
            tbody.appendChild(destinationRow);
          });
        }
      });

      table.appendChild(thead);
      table.appendChild(tbody);
      container.appendChild(col1);
      container.appendChild(col2);
      tableID.appendChild(table);
      tableID.appendChild(document.createElement('br')); 
    })
    .catch((error) => {
      console.log('Error retrieving agency data:', error);
    });
  });