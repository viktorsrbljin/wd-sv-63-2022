

const firebaseConfig = {
    apiKey: "AIzaSyC6faK1m1laCTZWsIdz0RLVH1F9civYDTU",
    authDomain: "turisticka-agencija-ac542.firebaseapp.com",
    databaseURL: "https://turisticka-agencija-ac542-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "turisticka-agencija-ac542",
    storageBucket: "turisticka-agencija-ac542.appspot.com",
    messagingSenderId: "395723141759",
    appId: "1:395723141759:web:b92d75933d48550146c54a",
    measurementId: "G-GXTFP2TW1E"
  };


firebase.initializeApp(firebaseConfig);
var database = firebase.database();


function updateUserImage() {
  var userImage = document.getElementById('userImage');

  if (window.loggedIn === false) {
    userImage.setAttribute('data-target', '#loginModal');
  } else if (window.loggedIn === true) {
    userImage.setAttribute('data-target', '#korisnikModal');
  }
}
updateUserImage()

function submitLoginForm(event) {
  event.preventDefault();
  document.getElementById('KorisnickoImeErrorMessage').style.display = 'none';
  document.getElementById('LozinkaErrorMessage').style.display = 'none';
  document.getElementById('loginSuccessMessage').style.display = 'none';

  var username = document.getElementById('username').value;
  var password = document.getElementById('password').value;

  var loginData = {
    username: username,
    password: password
  };

  
  var korisniciRef = database.ref('korisnici');
  korisniciRef.once('value', function(snapshot) {
    var korisnici = snapshot.val();

    var usernameExists = false;
    var correctPassword = false;

    
    for (var key in korisnici) {
      if (korisnici.hasOwnProperty(key)) {
        var korisnik = korisnici[key];

        
        if (korisnik.korisnickoIme === username) {
          usernameExists = true;

        
          if (korisnik.lozinka === password) {
            correctPassword = true;
            break;
          }
        }
      }
    }

    if (!usernameExists) {
      document.getElementById('KorisnickoImeErrorMessage').style.display = 'block';
    } else if (!correctPassword) {
      document.getElementById('LozinkaErrorMessage').style.display = 'block';
    } else {
      window.loggedIn = true;
      $('#loginModal').modal('hide');
      $('#korisnikModal').modal('show');
      updateUserImage()
    }
  });
}
document.getElementById('loginBtn').addEventListener('click', submitLoginForm);

function updateUserImage() {
  var userImage = document.getElementById('userImage');

  if (window.loggedIn === false) {
    userImage.setAttribute('data-target', '#loginModal');
  } else if (window.loggedIn === true) {
    userImage.setAttribute('data-target', '#korisnikModal');
  }
}


document.getElementById("logoutBtn").addEventListener("click", function() {
  window.loggedIn = false;
  updateUserImage();
  $('#korisnikModal').modal('hide');
});

function submitRegisterForm(event) {
    event.preventDefault(); 
  
    var usernameR = document.getElementById('usernameR').value;
    var passwordR = document.getElementById('passwordR').value;
    var imeR = document.getElementById('imeR').value;
    var prezimeR = document.getElementById('prezimeR').value;
    var emailR = document.getElementById('emailR').value;
    var rodjenjeR = document.getElementById('rodjenjeR').value;
    var adresaR = document.getElementById('adresaR').value;
    var telefonR = document.getElementById('telefonR').value;
  
    var korisnikData = {
      korisnickoIme: usernameR,
      lozinka: passwordR,
      ime: imeR,
      prezime: prezimeR,
      email: emailR,
      datumRodjenja: rodjenjeR,
      adresa: adresaR,
      telefon: telefonR,
    };

    database.ref('korisnici').push(korisnikData)
      .then(function() {
        console.log('Form data saved successfully.');
        document.getElementById('registerForm').reset(); 
        document.getElementById('successRegister').style.display = 'block'; 
      })
      .catch(function(error) {
        console.error('Error saving form data: ', error);
      });
  }
  document.getElementById('registerBtn').addEventListener('click', submitRegisterForm);


  const agencijeRef = firebase.database().ref('agencije');

  agencijeRef.on('value', (snapshot) => {
    const container = document.getElementById('prikazaneAgencijeContainer');
    container.innerHTML = '';
  
    let row = document.createElement('div');
    row.classList.add('row', 'justify-content-around');
  
    snapshot.forEach((childSnapshot, index) => {
      if (index % 3 === 0 && index > 0) {
        container.appendChild(row);
  
        row = document.createElement('div');
        row.classList.add('row', 'justify-content-around');
      }
  
      const agencija = childSnapshot.val();
  
      const col = document.createElement('div');
      col.classList.add('col-md-4', 'my-2');
      col.style.padding = '1rem';
  
      const link = document.createElement('a');
      link.href = `agencija.html?agencijaID=${childSnapshot.key}`;
  
      const card = document.createElement('div');
      card.classList.add('card', 'text-white', 'h-100', 'custom-card');
      card.style.backgroundColor = 'rgba(27, 53, 170, 0.65)';
      card.style.marginInline = '25px';
  
      const image = document.createElement('img');
      image.src = agencija.logo;
      image.style.maxWidth = '100%';
      image.style.height = '300px';
  
      const contentContainer = document.createElement('div');
      contentContainer.classList.add('content-container');
  
      const cardTitle = document.createElement('h2');
      cardTitle.classList.add('card-title');
      cardTitle.textContent = agencija.naziv;
  
      const cardBody = document.createElement('div');
      cardBody.classList.add('card-body', 'text-center');
      cardBody.appendChild(contentContainer);
  
      contentContainer.appendChild(image);
      contentContainer.appendChild(cardTitle);
  
      card.appendChild(cardBody);
      link.appendChild(card);
      col.appendChild(link);
      row.appendChild(col);
    });
  
    container.appendChild(row);
  });
  

  
  
  





  
