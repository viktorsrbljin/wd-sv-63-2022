const firebaseConfig = {
  apiKey: "AIzaSyC6faK1m1laCTZWsIdz0RLVH1F9civYDTU",
  authDomain: "turisticka-agencija-ac542.firebaseapp.com",
  databaseURL: "https://turisticka-agencija-ac542-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "turisticka-agencija-ac542",
  storageBucket: "turisticka-agencija-ac542.appspot.com",
  messagingSenderId: "395723141759",
  appId: "1:395723141759:web:b92d75933d48550146c54a",
  measurementId: "G-GXTFP2TW1E"
};


firebase.initializeApp(firebaseConfig);
var database = firebase.database();


function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}
updateUserImage()

function submitLoginForm(event) {
event.preventDefault();
document.getElementById('KorisnickoImeErrorMessage').style.display = 'none';
document.getElementById('LozinkaErrorMessage').style.display = 'none';
document.getElementById('loginSuccessMessage').style.display = 'none';

var username = document.getElementById('username').value;
var password = document.getElementById('password').value;

var loginData = {
  username: username,
  password: password
};


var korisniciRef = database.ref('korisnici');
korisniciRef.once('value', function(snapshot) {
  var korisnici = snapshot.val();

  var usernameExists = false;
  var correctPassword = false;

  
  for (var key in korisnici) {
    if (korisnici.hasOwnProperty(key)) {
      var korisnik = korisnici[key];

      
      if (korisnik.korisnickoIme === username) {
        usernameExists = true;

      
        if (korisnik.lozinka === password) {
          correctPassword = true;
          break;
        }
      }
    }
  }

  if (!usernameExists) {
    document.getElementById('KorisnickoImeErrorMessage').style.display = 'block';
  } else if (!correctPassword) {
    document.getElementById('LozinkaErrorMessage').style.display = 'block';
  } else {
    window.loggedIn = true;
    $('#loginModal').modal('hide');
    $('#korisnikModal').modal('show');
    updateUserImage()
  }
});
}
document.getElementById('loginBtn').addEventListener('click', submitLoginForm);

function updateUserImage() {
var userImage = document.getElementById('userImage');

if (window.loggedIn === false) {
  userImage.setAttribute('data-target', '#loginModal');
} else if (window.loggedIn === true) {
  userImage.setAttribute('data-target', '#korisnikModal');
}
}


document.getElementById("logoutBtn").addEventListener("click", function() {
window.loggedIn = false;
updateUserImage();
$('#korisnikModal').modal('hide');
});

function submitRegisterForm(event) {
  event.preventDefault(); 

  var usernameR = document.getElementById('usernameR').value;
  var passwordR = document.getElementById('passwordR').value;
  var imeR = document.getElementById('imeR').value;
  var prezimeR = document.getElementById('prezimeR').value;
  var emailR = document.getElementById('emailR').value;
  var rodjenjeR = document.getElementById('rodjenjeR').value;
  var adresaR = document.getElementById('adresaR').value;
  var telefonR = document.getElementById('telefonR').value;

  var korisnikData = {
    korisnickoIme: usernameR,
    lozinka: passwordR,
    ime: imeR,
    prezime: prezimeR,
    email: emailR,
    datumRodjenja: rodjenjeR,
    adresa: adresaR,
    telefon: telefonR,
  };

  database.ref('korisnici').push(korisnikData)
    .then(function() {
      console.log('Form data saved successfully.');
      document.getElementById('registerForm').reset(); 
      document.getElementById('successRegister').style.display = 'block'; 
    })
    .catch(function(error) {
      console.error('Error saving form data: ', error);
    });
}
document.getElementById('registerBtn').addEventListener('click', submitRegisterForm);

const urlParams = new URLSearchParams(window.location.search);
const destinacijaID = urlParams.get('destinacijaID');

const databaseRef = firebase.database().ref('destinacije');

databaseRef.once('value')
  .then((snapshot) => {
    let foundData = null;

    snapshot.forEach((destinacijaSnapshot) => {
      const destinacijaData = destinacijaSnapshot.val();

      Object.entries(destinacijaData).forEach(([nestedKey, nestedData]) => {
        if (nestedKey === destinacijaID) {
          foundData = nestedData;
        }
      });
    });

    if (foundData) {
      const carouselIndicators = document.querySelector('.carousel-indicators');
      const carouselInner = document.querySelector('.carousel-inner');

      foundData.slike.forEach((slika, index) => {
        const indicatorItem = document.createElement('li');
        indicatorItem.setAttribute('data-target', '#carouselIndicators');
        indicatorItem.setAttribute('data-slide-to', index);
        if (index === 0) {
          indicatorItem.classList.add('active');
        }
        carouselIndicators.appendChild(indicatorItem);

        const carouselItem = document.createElement('div');
        carouselItem.classList.add('carousel-item');
        if (index === 0) {
          carouselItem.classList.add('active');
        }

        const image = document.createElement('img');
        image.classList.add('d-block', 'w-100', 'img-fluid');
        image.style.height = '800px';
        image.src = slika;
        image.alt = 'Slide ' + (index + 1);

        carouselItem.appendChild(image);
        carouselInner.appendChild(carouselItem);
      });

      const podaciElement = document.getElementById('podaci');
      podaciElement.innerHTML = `
        ${foundData.tip}
        <br>${foundData.prevoz}
        <br>${foundData.cena}
        <br>${foundData.maxOsoba}
      `;

      const opisElement = document.getElementById('opis');
      opisElement.textContent = foundData.opis;

      const linkElement = document.getElementById('link');
      linkElement.href = `izmenaDestinacije.html?destinacijaID=${destinacijaID}`;
    } else {
      console.log('No data found for the specified destinacijaID');
    }
  })
  .catch((error) => {
    console.log('Error retrieving data:', error);
  });